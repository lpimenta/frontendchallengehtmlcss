
var gulp = require('gulp'),
    sass = require('gulp-sass'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    rename = require('gulp-rename'),
    minifyCss = require("gulp-minify-css"),
    clean = require('gulp-clean');


// Clean css files
gulp.task('clean-css', function () {
    return gulp.src('dist/*.css', {read: false})
        .pipe(clean());
});

// Clean js files
gulp.task('clean-js', function () {
    return gulp.src('dist/*.js', {read: false})
        .pipe(clean());
});
 
// Compile Our Sass
gulp.task('sass', ['clean-css'], function() {
    return gulp.src('sass/*.scss')
        .pipe(sass())
        .pipe(gulp.dest('css/generated/'));
});

// Css minify
gulp.task('minify-css', ['sass'], function () {
    return gulp.src('css/generated/*.css') // path to your file
        .pipe(rename('base.min.css'))
        .pipe(minifyCss())
        .pipe(gulp.dest('dist'));
});

// Concatenate & Minify JS
gulp.task('scripts', ['clean-js'],  function() {
    return gulp.src('js/app.js')
        .pipe(concat('all.js'))
        .pipe(gulp.dest('dist'))
        .pipe(rename('all.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('dist'));
});

// Watch Files For Changes
gulp.task('watch', function() {
    gulp.watch('js/*.js', ['scripts']);
    gulp.watch('sass/*.scss', ['sass']);
});

// Default Task
gulp.task('default', ['sass', 'minify-css', 'scripts', 'watch']);

gulp.task('deploy', ['minify-css', 'scripts']);
